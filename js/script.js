$(document).ready(function(){
    if($('body.page-search #main #main-inner #content-content').attr('id') != undefined){

        var solr   = '<div class="box">' + $('body.page-search #main #main-inner #content-content .box').html() + '</div>';
        var abbila = '<div class="block-block">' + $('body.page-search #main #main-inner #content-content .block-block').html() + '</div>';
        
        $('body.page-search #main #main-inner #content-content .box').remove();
        $('body.page-search #main #main-inner #content-content .block-block').remove();
        
        var form   = $('body.page-search #main #main-inner #content-content').html();

        $('body.page-search #main #main-inner #content-content').html(form + abbila + solr);
        
    }
});